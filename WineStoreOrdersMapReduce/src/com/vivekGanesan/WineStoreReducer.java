package com.vivekGanesan;

import java.io.IOException;
import org.apache.hadoop.io.DoubleWritable;
import java.util.Iterator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;



public class WineStoreReducer extends Reducer<Text, DoubleArrayWritable, Text, DoubleArrayWritable> {

    @Override
    public void reduce(Text key, Iterable<DoubleArrayWritable> values, Context output)
            throws IOException, InterruptedException {
    	double qSum = 0,tpSum=0,bvSum=0;
        for(DoubleArrayWritable value: values){
        	qSum+= value.getSingleValue(0);
            tpSum+=value.getSingleValue(1);
            bvSum+=value.getSingleValue(2);
        }
        double[] temp=new double[3];
        temp[0]=qSum;
        temp[1]=tpSum;
        temp[2]=bvSum/qSum;
        DoubleArrayWritable out=new DoubleArrayWritable(temp);
        output.write(key, out);
    }//reduce
    
    
    
}//reducer
