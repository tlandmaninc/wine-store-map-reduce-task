package com.vivekGanesan;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WineStoreMapper extends Mapper<Object, Text, Text, DoubleArrayWritable> {

    @Override
    public void map(Object key, Text value, Context output) throws IOException,
            InterruptedException {


        //If more than one word is present, split using white space.
        String[] record = value.toString().split("	");      
        java.util.List<String> tempRecord = Arrays.asList(record);
		java.util.List<Double> doubles = new ArrayList<Double>(); 
		double temp;
		
		//Converting String Array to Double Array
		for(int i=0; i < tempRecord.size();i++)
		{
			temp = Double.parseDouble(tempRecord.get(i));;
			doubles.add(new Double(temp));
		}
		
		/*Adding 2 Attributes: 
		 * Index 4: Total Price calculated by Quantity*Price Per product
		 * Index 5: Total Value calculated by Quantity*Value to business
		 */
		temp=doubles.get(3)*doubles.get(2);
		doubles.add(new Double(temp));
		temp=doubles.get(4)*doubles.get(2);
		doubles.add(new Double(temp));
		
		//Converting Double Array to String
		String[] mappedRecord = new String[doubles.size()];			
		for (int i = 0; i < 3; i++){
			temp=doubles.get(i);
			int iTemp=(int) temp;
			mappedRecord[i] = String.valueOf(iTemp);
		}				
		for (int i = 3; i < mappedRecord.length; i++)
			mappedRecord[i] = String.valueOf(doubles.get(i));
		
		double Quantity,TotalPrice,BusinessValue;
        Quantity=doubles.get(2);
        TotalPrice=doubles.get(5);
        BusinessValue=doubles.get(6);
        double ar[]=new double[3];
        ar[0]=Quantity;
        ar[1]=TotalPrice;
        ar[2]=BusinessValue;
        DoubleArrayWritable fin=new DoubleArrayWritable(ar);
        output.write(new Text(record[1]),fin);
    }//map    
}//mapper
